<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Log;
use app\models\Rider;
use app\models\YandexCookie;

class YandexController extends Controller
{
    public $yandex_server_url="https://taxi.yandex.ru/3.0/";
	public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'auth' => ['post'],
                ],
            ],
        ];
    }

    private function proxifyWithCookie($command) {
        $request = \Yii::$app->request;
        $body = $request->getRawBody();
        $id = json_decode($body)->{'id'};
        $cookie=$this->getCookieById($id);
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $this->yandex_server_url.$command.'/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Cookie: '.$cookie
    ));

        curl_setopt($ch, CURLOPT_POST,           1 );
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $body); 
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    private function getCookieById($id){
        $cookie = YandexCookie::find()->where(['key'=>$id])->one();
        return $cookie->cookie;
    }

    public function actionAuth()
    {
        return $this->proxifyWithCookie('auth');
    }

    public function actionAuthconfirm()
    {
        return $this->proxifyWithCookie('authconfirm');
    }
   
    public function actionLaunch()
    {
        return $this->proxifyWithCookie('launch');
    }  
    public function actionTaxisearch()
    {
        return $this->proxifyWithCookie('taxisearch');
    }

    public function actionRoutestats()
    {
        return $this->proxifyWithCookie('routestats');
    }

    public function actionOrder()    
    {
        return $this->proxifyWithCookie('order');
    }

    public function actionTaxiontheway()
    {
        return $this->proxifyWithCookie('taxiontheway');
    }
}
