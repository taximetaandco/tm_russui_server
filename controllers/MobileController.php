<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Log;
use app\models\Rider;
use app\models\City;
use app\models\Review;
use app\models\YandexCookie;
use app\models\ServiceTariff;
use app\models\GoogleDistanceToken;

class MobileController extends Controller
{
	public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'log' => ['post'],
                ],
            ],
        ];
    }

   

    public function actionSignGett()
    {
        $request = \Yii::$app->request;
        $req = $request->get('request');
        $code = $request->get('code');

        $salt = time();

        if(strrpos($req, "/server/")>0){
        	$req=substr($req,strrpos($req, "/server/"));
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        return [
                'success' => true,
                'salt' => $salt,
                'sign' => sha1($req."&salt=".$salt.$code)
            ];
    }

    public function actionYaToken(){
    	$url = "https://taxi.yandex.ru";
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);

curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:37.0) Gecko/20100101 Firefox/37.0',
    'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    ));
		$html = curl_exec($ch);
		curl_close($ch);

		$pos=strrpos($html, "\",\"id\":\"");
		$id = substr($html,$pos+8,32);


        preg_match_all('/^Set-Cookie:\s*([^\r]*)/mi', $html, $matches);


        $pos=strrpos($html, "Cookie");
        $cookieStr = implode(';', $matches[1]);

   
        $cookie = new YandexCookie();
        $cookie->cookie = $cookieStr;
        $cookie->key= $id;
        $cookie->save();



		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        return [
                'success' => true,
                'token' => $id,
                'cookie' => $cookieStr
            ];
    }

    public function actionDistanceMatrixKey(){
        $key = GoogleDistanceToken::find()->orderBy('RAND()')->one();
        $token = $key->distance_key;



        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
                'success' => true,
                'key' => trim($token)
            ];
        
    }


    public function actionLog(){
    	$request = \Yii::$app->request;

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $log = new Log();
        $log->phone = $request->post('phone'); 
        $log->data = $request->post('data');
        $log->type = $request->post('type');
        $log->created = new \yii\db\Expression('NOW()');
        $log->ip = $request->getUserIP();
        $log->save();

        return [
                'success' => true,
            ];
    }

    public function actionReview(){
        $request = \Yii::$app->request;

        $review = new Review();
        $review->phone = $request->get('phone'); 
        $review->text = $request->get('text');
        $review->rating = $request->get('rating');
        $review->created = new \yii\db\Expression('NOW()');
        $review->save();

        if($review->rating!=0&&$review->rating<4){
            $sended = \Yii::$app->mailer->compose()
                ->setFrom('cp@taximeta.com')
                ->setTo('service@bananataxi.ru')
                ->setSubject('Taximeta. Отзыв')
                ->setTextBody($review->phone."\n".$review->rating."\n".$review->text)
                ->send();
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
                'success' => true
            ];
    }

  

    public function actionTariffsText(){
		$request = \Yii::$app->request;
        $service = $request->get('service');
        $cityId = $request->get('cityId');
        if(!$cityId){
        	$cityId= 0;
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $tariff = ServiceTariff::find()->where(['serviceId'=>$service])->andWhere(['cityID'=>$cityId])->one();
        $text= $tariff->description;
        return [
                'success' => true,
                'text' => $text
            ];
    }

    public function actionOferta(){
        

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $text= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"><html><head>    <title></title> <meta name="generator" content="LibreOffice 4.4.2.2 (Linux)"/>  <meta name="author" content="BLP"/> <meta name="created" content="2015-12-08T22:39:00"/>    <meta name="changedby" content="Виталий Кириллов"/> <meta name="changed" content="2015-12-08T23:40:00"/>    <meta name="Capacities" content=""/>    <meta name="CreateDate" content="01.01.11"/>    <meta name="CustomFooter" content="Unprofiled Document"/>   <meta name="Generated" content="NO"/>   <meta name="LastEdit" content="01.01.11"/>  <meta name="Location" content="NEW"/>   <meta name="Parties" content=""/>   <meta name="SigCopy" content="0"/>  <meta name="Version" content="1.1"/>    <meta name="VersionCreated" content="01.01.11"/>    <meta name="ftRight" content="NEW"/>    <meta name="hdLeft" content="NEW"/> <meta name="hdRight" content="NEW"/>    <style type="text/css">     @page { margin-right: 3.17cm; margin-top: 2.54cm; margin-bottom: 2.54cm }       p { margin-left: 1.6cm; margin-top: 0.42cm; margin-bottom: 0cm; color: #000000; text-align: justify; orphans: 2; widows: 2 }        p.western { font-family: "Tahoma", sans-serif; font-size: 10pt; so-language: en-GB }        p.cjk { font-family: "Tahoma", sans-serif; font-size: 10pt }        p.ctl { font-family: "Tahoma", sans-serif; font-size: 10pt; so-language: ar-SA }        a:link { color: #5e163a }       a:visited { color: #5e163a }        a.western:visited { so-language: ru-RU }        a.cjk:visited { so-language: zh-CN }        a.ctl:visited { so-language: hi-IN }    </style></head><body lang="ru-RU" text="#000000" link="#5e163a" vlink="#5e163a" dir="ltr"><p lang="en-GB" class="western" style="margin-left: 0cm"><span lang="ru-RU"><i><b>Оферта на заключение лицензионного соглашенияи соглашения об оказании информационныхуслуг</b></i></span></p><p lang="en-GB" class="western" style="margin-left: 0cm"><span lang="ru-RU">ООО«Таксимета» - юридическое лицо, созданноев соответствии с законодательствомРоссийской Федерации. Полное фирменноенаименование: Общество с ограниченнойответственностью Таксимета, сокращенноефирменное наименование: Таксимета Местонахождения и почтовый адрес: 119334, Москва,ул. Вавилова, д.4, стр.2, помещение </span><span lang="en-US">V</span><span lang="ru-RU">,ком. 7, ОГРН 5157746090970, ИНН 7725297619, Email:</span><a class="western" href="mailto:info@taximeta.com"><font color="#5e163a"><u><span lang="en-US">info</span></u></font><font color="#5e163a"><u><span lang="ru-RU">@</span></u></font><font color="#5e163a"><u><span lang="en-US">taximeta</span></u></font><font color="#5e163a"><u><span lang="ru-RU">.</span></u></font></a><span lang="en-US">ru</span><span lang="ru-RU">, вебсайт:  </span><font color="#5e163a"><u><a class="western" href="http://www.taximeta.ru/"><span lang="ru-RU">www.taximeta.ru</span></a></u></font><span lang="ru-RU">или </span><a class="western" href="http://www.taximeta.com/"><font color="#5e163a"><u><span lang="en-US">www</span></u></font><font color="#5e163a"><u><span lang="ru-RU">.</span></u></font><font color="#5e163a"><u><span lang="en-US">taximeta</span></u></font><font color="#5e163a"><u><span lang="ru-RU">.</span></u></font><font color="#5e163a"><u><span lang="en-US">com</span></u></font></a><span lang="ru-RU">(далее – “Общество”) настоящим публикуетпубличную оферту.</span></p><p lang="en-GB" class="western" style="margin-left: 0cm"><span lang="ru-RU">Пользовательприложения “</span><span lang="en-US">Taximeta</span><span lang="ru-RU">”(далее – “Приложение”) используяПриложение соглашается на условиянастоящего лицензионного соглашенияи соглашения об оказании информационныхуслуг и дает акцепт на данную публичнуюоферту и связанные с ними документы,размещенные в Приложении или на сайте</span><font color="#5e163a"><u><a class="western" href="http://www.taximeta.ru/"><span lang="ru-RU">www.taximeta.ru</span></a></u></font><span lang="ru-RU">или </span><a class="western" href="http://www.taximeta.com/"><font color="#5e163a"><u><span lang="en-US">www</span></u></font><font color="#5e163a"><u><span lang="ru-RU">.</span></u></font><font color="#5e163a"><u><span lang="en-US">taximeta</span></u></font><font color="#5e163a"><u><span lang="ru-RU">.</span></u></font><font color="#5e163a"><u><span lang="en-US">com</span></u></font></a><span lang="ru-RU">(далее - “Соглашение”).</span></p><ol> <li/><p class="western">Общество через Приложение   предоставляет информационную услугу по выбору службы по перевозке пассажиров    автотранспорта в соответствии с критериями, выбранными Вами, из служб   такси и других приложений, а также из   информации агрегаторов заказов такси    и бирж заказов такси, отобранных    Обществом.</p>  <li/><p class="western">Услуги по перевозке осуществляются через выбранные Вами другие приложения и их партнеров. Вы    имеете право предъявлять Ваши претензии и требования непосредственно к  исполнителю услуги по перевозке.</p>    <li/><p lang="en-GB" class="western"><span lang="ru-RU">Лицензия    на данное Приложение предоставляется    на условиях “</span><span lang="ru-RU"><i>как есть</i></span><span lang="ru-RU">”   (то есть Общество не несет ответственности  за задержки, сбои, неверную или несвоевременную доставку, а также за    сбои в работе платежных систем).</span></p> <li/><p class="western">Владельцем Приложения   является Общество, включая все права    на интеллектуальную собственность,  связанные с Приложением.</p></ol><p class="western" style="margin-left: 1.27cm">Обществопредоставляет Вам неисключительнуюнекоммерческую лицензию без правапередачи и сублицензирования.</p><ol start="5">  <li/><p class="western">Вы не имеете права без  предварительного письменного согласия   Общества: (1) использовать, модифицировать, встраивать в другое программное обеспечение или объединять с ним,   создавать переработанную версию любой   части Приложения; (2) продавать, выдавать   лицензии (сублицензии), переуступать;   (3) копировать, распространять или  воспроизводить Приложение в интересах   третьих лиц, (4) модифицировать,    дизассемблировать, декомпилировать, разбирать на составляющие коды, перерабатывать или усовершенствовать    Приложение, пытаться получить исходный  текст программы Приложения.</p> <li/><p class="western"> Вы даете согласие на   автоматическое обновление Приложения    при появлении новых версий.</p> <li/><p class="western">Вы согласны на получение    смс-сообщений, необходимых для  использования Приложения.</p>   <li/><p class="western">Оплата за услуги конечных   исполнителей заказа осуществляется  посредством оплаты наличными денежными  средствами таким исполнителям или   привязки банковской карты в Приложении. Настоящим Вы соглашаетесь со списаниями средства с привязанной банковской   карты за осуществленные конечными   исполнителями услуг по перевозке.</p></ol><p class="western" style="margin-left: 0cm"><br/></p><ol start="9">   <li/><p class="western">Общество вправе направлять  Вам информацию о дополнительных услугах и обновлениях Приложения или информацию о совместных маркетинговых акциях с третьими лицами.</p>    <li/><p class="western">Вы соглашаетесь, что    Общество вправе обрабатывать Ваши   персональные данные для целей исполнения    Соглашения: номер телефона, данные  банковской карты, статистика поездок    и платежей. Данное согласие дано без    ограничения срока и без права отзыва.</p>   <li/><p class="western">Вы соглашаетесь с условиями оферты и другими документами третьих    лиц, непосредственно осуществляющих услуги по заказу и осуществлению    перевозок.</p>  <li/><p class="western">Ответственность Общества    за прямые или косвенные убытки от   использования Приложения ограничена 2,000 рублей.</p>   <li/><p class="western">Соглашение регулируется российским правом.  Споры из данного    Соглашения должны рассматриваться в компетентных судах г. Москвы.</p></ol></body></html>';
         return [
                'success' => true,
                'text' => $text
            ];
    }



    public function actionUberClient(){



        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
          return [
                    'success' => true,
                    'client_id' => "fWf7wIui2iwuMAihx0ma6NsK43SHQA9i",
                    'redirect_uri' => "https://workflow.is/api/uber/v1/callback"
                ];
    }

    public function actionSmsPatterns(){



        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
          return [
                    'success' => true,
                    'patterns' => [
                        1=>['Gett код: ([0-9]+).','Gett код: ([0-9]+).\nGetTaxi'],
                        2=>['Код для входа в Яндекс.Такси: ([0-9]+)'],
                        4=>['Код активации приложения: ([0-9]+)\nNowTaxi.RU', 'Код активации приложения: ([0-9]+)'],
                        5=>['Код ([0-9]+)','Kod ([0-9]+)']
                    ]
                ];
    }


    public function actionCheckVersion(){

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
          return [
                    'success' => true,
                    'need_update' => false
                ];
    }

    public function actionZoozUdid(){



        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
          return [
                    'success' => true,
                    'udid' => 'vendor_2A42E161-52C1-4239-815E-2EF3F30AEE66',
                    'alt_udid' => '7E6C402D-42C2-4FA1-8DD8-A790E94DDE71'
                ];
    }

    public function actionConfig(){
        $cities = City::find()->all();
         $json = [];
        foreach ($cities as $city) {
            $map = [];
            $map['id']=$city->id;
            $map['name']=$city->name;
            $map['lat']=$city->lat;
            $map['lon']=$city->lon;
            $map['radius']=$city->radius;
            $map['airports']=[];

            foreach ($city->getAirports()->all() as $airport) {
                $airportMap = [];
                $airportMap['name']=$airport->name;
                $airportMap['lat']=$airport->lat;
                $airportMap['lon']=$airport->lon;
                $airportMap['radius']=$airport->radius;
                $map['airports'][]=$airportMap;
            }

            if($city->id==0){
                $map['services']=[1,2,3,4,5,6];
            }else if($city->id==1) {
                $map['services']=[1,2,3];
            }else if($city->id==2) {
                $map['services']=[7,8];
            }
            $json[]=$map;
        }

       



        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
              return [
                    'success' => true,
                    'cities' => $json
                ];
    }

    public function actionStartupBanner(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
              return [
                    'success' => true,
                    'enabled' => false,
                    'imageUrl' => 'http://forum.tomsk.ru/attaches/1490808/30920422/13848808081996.gif',
                  
                ];
    }

    public function actionUberToken(){

		$request = \Yii::$app->request;
        $code = $request->get('code');


        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"https://workflow.is/api/uber/v1/oauth/token");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('code' => $code, 'client_id' => "fWf7wIui2iwuMAihx0ma6NsK43SHQA9i", 'redirect_uri' => "https://workflow.is/api/uber/v1/callback", 'grant_type' => 'authorization_code')));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);

        curl_close ($ch);

        return $server_output;

 }


    public function actionTime(){
        $request = \Yii::$app->request;
        $service = $request->get('service');
        $lat = $request->get('lat');
        $lon = $request->get('lon');



        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($service==1){
            $code = "9bd4c23060510133c23922000ac5845b";
            $url = "https://ru.gett.com/server/3_1/phone/79852361733/available_divisions?lat=".$lat."&lon=".$lon."&lc=ru";
            $salt = time();
            $req=substr($url,strrpos($url, "/server/"));
            $sign = sha1($req."&salt=".$salt.$code);
            $url.="&salt=".$salt."&sig=".$sign;


            $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $json = curl_exec($ch);
            curl_close($ch);
            $divisions = json_decode($json)->{'divisions'};
            $time = [];

            foreach ($divisions as $division) {
                $time[] = array('service'=>$division->{'id'}, 'seconds'=>$division->{'eta'});
            }

            return [
                    'success' => true,
                    'time' => $time
                ];
        }  else {
            return [
                    'success' => false,
                ];
        }
    }

    public function actionPrice(){
        $request = \Yii::$app->request;
        $service = $request->get('service');
        $time = $request->get('time')/60;
        $distance = $request->get('distance')/1000;
        $cityId=0;
        if($request->get('cityId')){
            $cityId=$request->get('cityId');
        }


        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($service==1){
            if($cityId==0){
                return [
                    'success' => true,
                    'prices' => ['41'=>($time<=10)?149:(149+($time-10)*11),'24'=>(50+$time*18), '28'=>(200+$time*18), '26'=>($time<=5)?299:(299+($time-5)*29), '29'=>($time<=5)?499:(499+($time-5)*46), '27'=>($time<=5)?299:(299+($time-5)*19)]
                ];
            }else {
                return [
                    'success' => true,
                    'prices' => ['30'=>(50+$time*16), '35'=>(200+$time*18), '32'=>($time<=5)?299:(349+($time-5)*20), '33'=>($time<=5)?499:(499+($time-5)*35), '34'=>($time<=5)?299:(299+($time-5)*30)]
                ];
            }
        } else if($service==4) {
            $offset=3*60*60; //converting 3 hours to seconds.
            $hours = date("H", time()+$offset);
            if($distance<5){
                $distance=0;
            }else {
                $distance-=5;
            }
            if($hours>=8&&$hours<21){ //day mode
                return [
                    'success' => true,
                    'prices' => ['standard'=>($time<=10)?199:(199+($time-10)*14),'comfort'=>($time<=15)?299:(299+($time-10)*16), 'business'=>($time<=30)?750:(750+($time-30)*25)]
                ];
            }else {
                return [
                    'success' => true,
                    'prices' => ['standard'=>($time<=10)?199:(199+($time-10)*15)+$distance*8,'comfort'=>($time<=15)?299:(299+($time-10)*16)+$distance*8, 'business'=>($time<=10)?399:(299+($time-10)*25)]
                ];
            }
            
        }else if($service==5) {
            $offset=3*60*60; //converting 3 hours to seconds.
            $hours = date("H", time()+$offset);
            $standard=0;
            if(($hours>=6&&$hours<=9)||($hours>=16&&$hours<=21)){
                $standard=($distance<=3)?320:(320+($distance-3)*22);
            }else if($hours>=0&&$hours<=6){
                $standard=($distance<=5)?250:(250+($distance-5)*18);
            }else {
                $standard=($distance<=3)?270:(270+($distance-3)*20);
            }
            $comfort=0;
            if(($hours>=6&&$hours<=9)||($hours>=16&&$hours<=21)){
                $comfort=($distance<=3)?350:(350+($distance-3)*25);
            }else {
                $comfort=($distance<=3)?330:(330+($distance-3)*25);
            }
            return [
                'success' => true,
                'prices' => ['4'=>$standard,'1'=>$comfort, '2'=>(650+$distance*25), '3'=>($distance<=3)?330:(330+($distance-3)*25)]
            ];
          
            
        } else {
            return [
                    'success' => false,
                ];
        }
    }
}
