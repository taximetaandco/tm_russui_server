<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Log;
use app\models\Rider;
use app\models\YandexCookie;
use app\models\ServiceTariff;
use app\models\OrderLog;

class LogController extends Controller
{
	public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'log' => ['post'],
                ],
            ],
        ];
    }

    public function actionLogin()
    {
        $request = \Yii::$app->request;
        $phone = $request->get('phone');
        $platform = $request->get('platform');
        $token = $request->get('udid');


        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $rider = Rider::find()->where(['phone'=>$phone])->one();
        if(!$rider){
            $rider = new Rider();
            $rider->phone = $phone;
            $rider->created = new \yii\db\Expression('NOW()');
            
        }
        $rider->lastLogin = new \yii\db\Expression('NOW()');
        $rider->platform=($platform=='ios')?1:0;
        $rider->token=$token;
        $rider->save();

        return [
                'success' => true,
            ];
    }

    
    public function actionOrder(){

        $request = \Yii::$app->request;

       
        $street = $request->get('street');
        $home = $request->get('home');
        $phone = $request->get('phone');
        $lat = $request->get('lat');
        $lon = $request->get('lon');
        $name = $request->get('name');
        $dstreet = $request->get('dstreet');
        $dhome = $request->get('dhome');
        $dlat = $request->get('dstLat');
        $dlon = $request->get('dstLon');
        $service;
        if($request->get('service')){
            $service = $request->get('service');
        }else {
            $service = $request->get('api');
        }
        if($service=='6'){
            return [
                'success' => true,
            ];
        }
        $due = $request->get('due');

        $phone = str_replace("-","",str_replace(" ","",str_replace(")","",str_replace("(","",$phone))));

        $log = new OrderLog();
        $log->dstLat = $dlat;
        $log->lat = $lat;
        $log->dstLon= $dlon;
        $log->lon= $lon;
        $log->phone= $phone;
        $log->service= $service; 
        $log->status= 0;
        $log->due= $due;
        $log->created= date("Y-m-d H:i:s");
        $log->save();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
                'success' => true,
            ];
    }

    public function actionOrderCancel(){

        $request = \Yii::$app->request;
        $phone = $request->get('phone');
        $phone = str_replace("-","",str_replace(" ","",str_replace(")","",str_replace("(","",$phone))));

        $item = OrderLog::find()->where(['phone'=>$phone])->orderBy(['id' => SORT_DESC])->one();
        $item->status=OrderLog::STATUS_CANCELLED;
        $item->save();

   

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
                'success' => true,
            ];
    }
}