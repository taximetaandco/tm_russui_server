<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Log;
use app\models\Rider;
use app\models\OrderLog;

class OrderController extends Controller
{
	public $enableCsrfValidation = false;
    public $upupkey = "3fa19d6e89da4b41baec5408874987f6";
    public $upupserver = "http://upup.su/api";
    public $upuplogin = "bananataxi";
    public $upuppassword = "dskugEWGU_382Ddb";

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'new' => ['post'],
                    'status' => ['post','get'],
                    'cancel' => ['post'],
                ],
            ],
        ];
    }

    private function distance($lat1, $lon1, $lat2, $lon2) {

      $theta = $lon1 - $lon2;
      $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
      $dist = acos($dist);
      $dist = rad2deg($dist);
      $miles = $dist * 60 * 1.1515;

     
            return $miles;
          
    }

   

    public function actionNew(){
    	$request = \Yii::$app->request;

       
        $street = $request->post('street');
        $home = $request->post('home');
        $phone = $request->post('phone');
        $lat = $request->post('lat');
        $lon = $request->post('lon');
        $name = $request->post('name');
        $dstreet = $request->post('dstreet');
        $dhome = $request->post('dhome');
        $dlat = $request->post('dlat');
        $dlon = $request->post('dlon');
        $price = $request->post('price');

        if($price<51){
            return "error";
        }


        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $city='МОСКВА';

        if($this->distance($lat,$lon,55.7500,37.6167)>$this->distance($lat,$lon,59.9500,30.3000)){
            $city='САНКТ-ПЕТЕРБУРГ';
        }

        

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$this->upupserver);
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('v' => '1.5.13', 'key' => $this->upupkey, 'character' => 'utf-8', 'controller' => 'order', 'function' => 'add', 'city' => $city, 'street' => $street, 'home' => $home, 'porch' => '', 'block' => '', 'structure' => '', 'dcity' => $city, 'dstreet' => $dstreet, 'dhome' => $dhome, 'dporch' => '', 'dblock' => '', 'dstructure' => '', 'children' => '0', 'name' => $name, 'nosmoking' => '0', 'smoking' => '0', 'card' => '0', 'animal' => '0', 'conditioner' => '0', 'beznal' => '0', 'number' => $phone, 'carWithLicense' => '0', 'comment' => '', 'deadline' => (time()+60*30), 'tarif' => 'e/f/'.$price, 'lat' => $lat, 'lon' => $lon, 'dlat' => $dlat, 'dlon' => $dlon,  'json' => 1,  'external_id' => md5(time()))));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);

        curl_close ($ch);
        $decoded = json_decode($server_output);

       

        if(property_exists($decoded ,'id')){
            $id = ($decoded->{'id'}); 
            $log = new OrderLog();
            $log->dstLat = $dlat;
            $log->lat = $lat;
            $log->dstLon= $dlon;
            $log->lon= $lon;
            $log->phone= $phone;
            $log->inner_id=$id."";
            $log->service= 6;
            $log->status= OrderLog::STATUS_ROUTING;
            $log->price= $price;
            $log->created= date("Y-m-d H:i:s");
            $log->save();
          return [
                'success' => true,
                'id' => $id
            ];
        }else {
             return [
                'success' => false,
                'response' => $server_output
            ];
        }

      
    }

    private function getDriverPhoneByOrderId($orderId){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,'http://upup.su/logon.do');
        curl_setopt($ch, CURLOPT_POST,           1 );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('login' => $this->upuplogin, 'password'=>$this->upuppassword)));

        $server_output = curl_exec ($ch);
        preg_match_all('/^Set-Cookie:\s*([^\n]*)/mi', $server_output, $matches);

        $pos=strrpos($server_output, "JSESSIONID=");
        $id = substr($server_output,$pos+11,32);


        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,'http://upup.su/userOrders.do?id='.$orderId.'&orderPopupFormType=1&rand=0.020023005278136052&action=commons.showOrderSpecificationForm');
        $cookie='JSESSIONID='.$id;

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Cookie: '.$cookie
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);

        $html = \garyjl\simplehtmldom\SimpleHtmlDom::str_get_html($server_output);
        $ret = $html->find('input[name="orderSpec.orderDriverPhone"]')[0]; 
        return $ret->value;
    }

    public function actionTime(){
    	return date('Y-m-j H:m:s.0',$timestamp).$timestamp;
    }

    public function actionExchanger(){
        $request = \Yii::$app->request;

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $settings = Yii::$app->settings;
        $enabled = $settings->get('exchanger','enabled');
        if($enabled==null){
            $enabled=true; 
        }else {
            $enabled=$enabled=='true';
        }
       
        return [
                'success' => true,
                'enabled' => $enabled
            ];
    }

    public function actionStatus(){
        $request = \Yii::$app->request;
       
        $id= $request->post('id');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"http://upup.su/api");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('v' => '1.5.13', 'key' => $this->upupkey, 'character' => 'utf-8', 'controller' => 'order', 'function' => 'info', 'id' => $id, 'json'=>1)));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);
 
        curl_close ($ch);

        $json = json_decode($server_output, true);
        $order= $json['order'];

        $phone=$this->getDriverPhoneByOrderId($id);
        $json['order']['driver_phone']=$phone;
        //array_push(['driver_phone'=>$phone], $order);

        $result = json_encode($json);

        $status = $json['order']['status'];
        $phone =  $json['order']['client']['number'];
        if(substr($phone,0,1)=='+'){
            $phone=substr($phone,1);
        }
        elseif(substr($phone,0,1)!='7'){
            $phone='7'.$phone;
        }

        $status_code=0;
        if($status == "start"||$status == "auction"){
            $status_code = OrderLog::STATUS_ROUTING;
        } else if($status == "work"||$status == "move"||$status == "client"){
            $status_code = OrderLog::STATUS_CONFIRMED;
        } else if($status == "client-move"){
            $status_code = OrderLog::STATUS_RIDING;
        } else  if($status == "done"){
            $status_code = OrderLog::STATUS_FINISHED;
        } else if($status == "cancel"||$status == "fail"||$status == "delete"){
            $status_code = OrderLog::STATUS_CANCELLED;
        }

        $item = OrderLog::find()->where(['phone'=>$phone])->orderBy(['id' => SORT_DESC])->one();
        if($item) {
            if($status_code==OrderLog::STATUS_CONFIRMED&&$item->status!=$status_code){
                $item->confirmed_time=date("Y-m-d H:i:s");
            }
            $item->status=$status_code;
            $item->save();

        }

        return $result;
    }


    public function actionCancel(){
        $request = \Yii::$app->request;

       \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
       
        $id= $request->post('id');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"http://upup.su/api");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('v' => '1.5.13', 'key' => $this->upupkey, 'character' => 'utf-8', 'controller' => 'order', 'function' => 'del', 'id' => $id, 'json'=>1)));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);

        curl_close ($ch);

        $item = OrderLog::find()->where(['inner_id'=>$id])->one();
        if($item&&$item->status!=OrderLog::STATUS_FINISHED) {
            $item->status=OrderLog::STATUS_CANCELLED;
            $item->canceled_time=date("Y-m-d H:i:s");
            $item->save();

        }

 
        return [
                'success' => true,
              //  'id' => $id
            ];
    }

    
}
