<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Rider;

class PushController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','index','users'],
                'rules' => [
                    [
                        'actions' => ['logout','index','users'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }




 
    public function actionSend()
    {
    /*    $apns = Yii::$app->apns;
        $riders = Rider::find()->where(['not', ['token' => null]])->all();
        $tokens = array();
        foreach ($riders as $rider) {
           // echo $rider->token;
            if($rider->token=="(null)"){ 
                continue;
            }
           $tokens[]=str_replace(" ","",str_replace(">","",str_replace("<","","$rider->token")));
        }
        
        $apns->sendMulti($tokens, 'Мы увеличили парк машин более чем в 2 раза! Теперь такси доступно еще быстрее! Спасибо, что вы с нами!',
          [
            'customProperty_1' => 'Hello',
            'customProperty_2' => 'World'
          ],
          [
            'sound' => 'default',
            'badge' => 0
          ]
        );*/
        return $this->render('send');
    }
}
