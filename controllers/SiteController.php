<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use app\models\LoginForm; 
use app\models\BudgetForm; 
use app\models\Rider;
use app\models\Log;
use app\models\OrderLog;
use app\models\Review;
use app\models\Budget;
use app\models\ServiceTariff;
use yii\db\Query;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','index','users'],
                'rules' => [
                    [
                        'actions' => ['logout','index','users'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $query = new Query;
        // compose the query
        $query->select('SUM( price) revenue')
            ->from('order_log')
            ->where(['status' => OrderLog::STATUS_FINISHED]);
        $rev = $query->one();
        $totalRevenue=round($rev['revenue']*0.08);

        $orderCount = OrderLog::find()->count();
        $finishedOrderCount = OrderLog::find()
            ->where(['status' => OrderLog::STATUS_FINISHED])
            ->count();
        $upupOrderCount = OrderLog::find()
            ->where(['status' => OrderLog::STATUS_FINISHED])
            ->andWhere(['service' => 6])
            ->count();
        $riderCount = Rider::find()->count();
        $riderIOSCount = Rider::find()->where(['platform'=>1])->count();
        $riderAndroidCount = Rider::find()->where(['platform'=>0])->count();
        $settings = \Yii::$app->settings;
        $iosInstalls = $settings->get('stat','ios-unique-users');
        $androidInstalls = $settings->get('stat','android-unique-users');
        return $this->render('dashboard', ['upupOrderCount'=>$upupOrderCount,'finishedOrderCount'=>$finishedOrderCount,'orderCount'=>$orderCount,'iosInstalls'=>$iosInstalls,'androidInstalls'=>$androidInstalls,'riderIOSCount'=>$riderIOSCount,'riderAndroidCount'=>$riderAndroidCount,'riderCount'=>$riderCount, 'totalRevenue'=>$totalRevenue+1460]);
    }

    public function actionLog($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Log::find()->where(['phone'=>$id])->orderBy('created DESC'),
        ]);
        return $this->render('log', ['dataProvider'=>$dataProvider]);
    }


    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionUsers()
    {
        $riders = Rider::find()->all();
        return $this->render('users', ['riders'=>$riders]);
    }

    public function actionAnalytics()
    {

        $query = new Query;
        // compose the query

        $query->select('COUNT(DISTINCT phone) as count')
            ->from('order_log');
            $query->andWhere(['status' => OrderLog::STATUS_FINISHED]);

        $row = $query->one();
   
$activeUsers = $row['count']; 

         $query = new Query;
        // compose the query

        $query->select('COUNT(DISTINCT phone) as count')
            ->from('order_log')
            ->where(['service' => 6]);
            $query->andWhere(['status' => OrderLog::STATUS_FINISHED]);

        $row = $query->one();
   
    $exchangerUsers = $row['count']; 
     
     $query = new Query;
            // compose the query  
     $connection = Yii::$app->getDb();
    $command = $connection->createCommand('SELECT count(phone) as count from (SELECT phone, COUNT(id) as c FROM `order_log` WHERE service = 6 and status = :status group by phone order by c desc) as rides where c>1', [':status' => OrderLog::STATUS_FINISHED]);
     
    $result = $command->queryOne();


    $retRiders = $result['count'];
 
    $query = new Query;
        // compose the query
        $query->select('SUM( price) revenue')
            ->from('order_log')
            ->where(['status' => OrderLog::STATUS_FINISHED]);
        $rev = $query->one();
        $totalRevenue=round($rev['revenue']*0.08);

          $query = new Query;
        // compose the query
        $query->select('AVG( price) revenue')
            ->from('order_log')
            ->where(['status' => OrderLog::STATUS_FINISHED]);
        $rev = $query->one();
        $avgCPR=round($rev['revenue']);

$avgRevenue = round($totalRevenue/$exchangerUsers);

      // compose the query  
 $connection = Yii::$app->getDb();
$command = $connection->createCommand('SELECT phone, COUNT(id) as rides, SUM(price)*0.08 as price FROM `order_log` WHERE service = 6 and status = :status group by phone order by rides desc ', [':status' => OrderLog::STATUS_FINISHED]);
 
$riders = $command->queryAll();


        $settings = \Yii::$app->settings;
        $installs = $settings->get('stat','ios-unique-users')+$settings->get('stat','android-unique-users');

        return $this->render('analytics', ['installs'=>$installs,'activeUsers'=>$activeUsers,'exchangerUsers'=>$exchangerUsers,'avgCPR'=>$avgCPR,'retRiders'=>$retRiders,'avgRevenue'=>$avgRevenue,'riders'=>$riders]);
    }


    private function getOrderStat($service, $completed=false, $other = false){
         $query = new Query;
        // compose the query

        $query->select('DATE(created) date, COUNT(DISTINCT id) count')
            ->from('order_log');
        if($other){
            $query->where(['not', ['service' => $service]]);
        }else {
            $query->where(['service' => $service]);
        }
            
        if($completed){
            $query->andWhere(['status' => OrderLog::STATUS_FINISHED]);
        }
        $query->groupBy('DATE(created)');
        // build and execute the query
        $rows = $query->all();
        // alternatively, you can create DB command and execute it
        $command = $query->createCommand();
        // $command->sql returns the actual SQL
        $rows = $command->queryAll();
        $resultOrders=[];
        foreach ($rows as $row) {
            $resultOrders[]=[strtotime($row['date'])*1000 +3*60*60*1000,intval($row['count'])];
        }

        return $resultOrders;

    }

    public function actionStat()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $orders=[];
        $orders['all']['Gett']=$this->getOrderStat(1);
        $orders['all']['Yandex']=$this->getOrderStat(2);
        $orders['all']['Uber']=$this->getOrderStat(3);
        $orders['all']['Now']=$this->getOrderStat(4);
        $orders['all']['Maxim']=$this->getOrderStat(5);
        $orders['all']['Exchanger']=$this->getOrderStat(6);
        $orders['all']['NotExchanger']=$this->getOrderStat(6,false,true);

        $orders['completed']['Gett']=$this->getOrderStat(1,true);
        $orders['completed']['Yandex']=$this->getOrderStat(2,true);
        $orders['completed']['Uber']=$this->getOrderStat(3,true);
        $orders['completed']['Now']=$this->getOrderStat(4,true);
        $orders['completed']['Maxim']=$this->getOrderStat(5,true);
        $orders['completed']['Exchanger']=$this->getOrderStat(6,true);
        $orders['completed']['NotExchanger']=$this->getOrderStat(6,true,true);
       
        $query = new Query;
        // compose the query
        $query->select('DATE(created) date, SUM( price) revenue')
            ->from('order_log')
            ->where(['status' => OrderLog::STATUS_FINISHED])
            ->groupBy('DATE(created)');
        // build and execute the query
        $rows = $query->all();
        $resultRevenue=[];
        foreach ($rows as $row) {
            $resultRevenue[]=[strtotime($row['date'])*1000+3*60*60*1000,intval($row['revenue'])*0.08];
        }



        $settings = \Yii::$app->settings;

        $resultActiveAndroid=json_decode($settings->get('stat','android-active-users'));
        $resultActiveIOS=json_decode($settings->get('stat','ios-active-users'));

        $query = new Query;
        // compose the query
        $query->select('DATE(created) date, COUNT( id) count')
            ->from('rider')
            ->where(['platform'=>0])
            ->groupBy('DATE(created)');
        // build and execute the query
        $rows = $query->all();
        $resultNew=[];
        foreach ($rows as $row) {
            $resultNew[]=[strtotime($row['date'])*1000+3*60*60*1000,intval($row['count'])];
        }

        $query = new Query;
        // compose the query
        $query->select('DATE(created) date, COUNT( id) count')
            ->from('rider')
            ->where(['platform'=>1])
            ->groupBy('DATE(created)');
        // build and execute the query
        $rows = $query->all();
        $resultNewIOS=[];
        foreach ($rows as $row) {
            $resultNewIOS[]=[strtotime($row['date'])*1000+3*60*60*1000,intval($row['count'])];
        }

        $query = new Query;
        // compose the query
        $query->select('lat, lon, dstLat, dstLon')
            ->from('order_log')
            ->where(['status' => OrderLog::STATUS_FINISHED]);
        // build and execute the query
        $rows = $query->all();
        $resultPoints=[];
        foreach ($rows as $row) {
            $resultPoints[]=[floatval($row['lat']),floatval($row['lon'])];
            $resultPoints[]=[floatval($row['dstLat']),floatval($row['dstLon'])];
        }

        $query = new Query;
        $query->select('DATE(created) date, AVG(TIME_TO_SEC(canceled_time)-TIME_TO_SEC(created)) time')
            ->from('order_log')
            ->where(['not', ['canceled_time' => null]])
            ->groupBy('DATE(created)');
        $rows = $query->all();
        $times=[];
        foreach ($rows as $row) {
            $times['canceled'][]=[strtotime($row['date'])*1000+3*60*60*1000,intval($row['time'])];
        }

        $query = new Query;
        $query->select('DATE(created) date, AVG(TO_SECONDS(confirmed_time)-TO_SECONDS(created)) time')
            ->from('order_log')
            ->where(['not', ['confirmed_time' => null]])
            ->groupBy('DATE(created)');
        $rows = $query->all();
        foreach ($rows as $row) {
            $times['confirmed'][]=[strtotime($row['date'])*1000+3*60*60*1000,intval($row['time'])];
        }

        $budget = [];
        $budgetItems = Budget::find()->all();
        foreach ($budgetItems as $item) {
            $budget[]=[strtotime($item['start'])*1000+3*60*60*1000,intval($item['price'])];
        }


        
        return [
        	'currentDateTime'=>date("Y-m-d H:i:s"),
            'orders'=>$orders,
            'revenue'=>$resultRevenue,
            'activeAndroid'=>$resultActiveAndroid,
            'activeIOS'=>$resultActiveIOS,
            'newAndroid'=>$resultNew,
            'newIOS'=>$resultNewIOS,
            'points'=>$resultPoints,
            'times'=>$times,
            'budget'=>$budget
        ];
    }


    public function actionOrders()
    {
        $orders = OrderLog::find()
            ->where(['service' => 6])
            ->andWhere(['status' => OrderLog::STATUS_FINISHED])
            ->orderBy('created DESC')
            ->all();
        return $this->render('orders', ['orders'=>$orders]);
    }

    public function actionBudget()
    {
        $model = new BudgetForm();
        if ($model->load(Yii::$app->request->post()) && $model->create()) {
            $model = new BudgetForm();
        }
        $items = Budget::find()
            ->all();
        return $this->render('budget', ['items'=>$items,'model' => $model]);
    }

    public function actionDeletebudget($id)
    {
        
        $budget = Budget::find()->where(['id'=>$id])->one();
        $budget->delete();
        return $this->redirect(['site/budget']);
    }

    public function actionReviews()
    {
        $orders = Review::find()
            ->orderBy('created DESC')
            ->all();
        return $this->render('reviews', ['reviews'=>$orders]);
    }

    public function actionTariffs()
    {
        $moscowTariffs = ServiceTariff::find()->where(['cityId'=>0])->all();
        $piterTariffs = ServiceTariff::find()->where(['cityId'=>1])->all();
        return $this->render('tariffs', ['moscowTariffs'=>$moscowTariffs,'piterTariffs'=>$piterTariffs]);
    }

    public function actionSettings()
    {
        $request = \Yii::$app->request;
        if($request->get('exchanger')!=null){

            $settings = Yii::$app->settings;
            $settings->set('exchanger','enabled',$request->get('exchanger'));
        }
        return '';
    }

    public function actionTariff($id)
    {

        $tariff = ServiceTariff::find()->where(['id'=>$id])->one();
        if($tariff->load(Yii::$app->request->post())){
            $tariff->save();
            return $this->redirect(['site/tariffs']);
        }
        return $this->render('tariff', ['tariff'=>$tariff]);
    }
}
