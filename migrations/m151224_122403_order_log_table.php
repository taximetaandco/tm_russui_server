<?php

use yii\db\Schema;
use yii\db\Migration;

class m151224_122403_order_log_table extends Migration
{
    public function up()
    {
		$this->createTable('order_log', array(
            'id' => 'pk',
            'phone' => 'string',
            'created' => 'datetime',
            'lat' => 'double',
            'lon' => 'double',
            'dstLat' => 'double',
            'dstLon' => 'double',
            'service' => 'int',
            'ip' => 'string',
            'comfort_class' => 'string',
            'price' => 'int',
            'status' => 'int',
            'due' => 'long',
            'driver_phone' => 'string',
            'taxi_service_name' => 'string'
        ));
    }

    public function down()
    {
        echo "m151224_122403_order_log_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
