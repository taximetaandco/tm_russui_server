<?php

use yii\db\Schema;
use yii\db\Migration;

class m151212_113622_service_tariff extends Migration
{
    public function up()
    {
        $this->createTable('service_tariff', array(
            'id' => 'pk',
            'serviceId' => 'int',
            'description' => 'text',
            'serviceName' => 'string'
        ));
    }

    public function down()
    {
        echo "m151212_113622_service_tariff cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
