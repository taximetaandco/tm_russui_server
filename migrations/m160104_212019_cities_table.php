<?php

use yii\db\Schema;
use yii\db\Migration;

class m160104_212019_cities_table extends Migration
{
    public function up()
    {
        $this->createTable('city', array(
            'id' => 'pk',
            'name' => 'string',
            'lat' => 'double',
            'lon' => 'double',
            'radius' => 'int'
        ));
    }

    public function down()
    {
        echo "m160104_212019_cities_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
