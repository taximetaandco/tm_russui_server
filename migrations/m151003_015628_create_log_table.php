<?php

use yii\db\Schema;
use yii\db\Migration;

class m151003_015628_create_log_table extends Migration
{
    public function up()
    {
        $this->createTable('log', array(
            'id' => 'pk',
            'phone' => 'string',
            'created' => 'datetime',
            'data' => 'text',
            'type' => 'string',
            'ip' => 'string'
        ));
    }

    public function down()
    {
        echo "m151003_015628_create_log_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
