<?php

use yii\db\Schema;
use yii\db\Migration;

class m151230_003649_add_platform_column extends Migration
{
    public function up()
    {
        $this->addColumn('rider','platform','int');
    }

    public function down()
    {
        echo "m151230_003649_add_platform_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
