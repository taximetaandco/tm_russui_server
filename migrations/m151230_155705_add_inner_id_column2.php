<?php

use yii\db\Schema;
use yii\db\Migration;

class m151230_155705_add_inner_id_column2 extends Migration
{
    public function up()
    {

        $this->addColumn('order_log','inner_id','string');
    }

    public function down()
    {
        echo "m151230_155705_add_inner_id_column2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
