<?php

use yii\db\Schema;
use yii\db\Migration;

class m160120_190018_add_columns_to_rider_log_table extends Migration
{
    public function up()
    {

        $this->addColumn('order_log','confirmed_time','datetime');
        $this->addColumn('order_log','canceled_time','datetime');
    }

    public function down()
    {
        echo "m160120_190018_add_columns_to_rider_log_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
