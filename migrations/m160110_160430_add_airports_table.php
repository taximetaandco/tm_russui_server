<?php

use yii\db\Schema;
use yii\db\Migration;

class m160110_160430_add_airports_table extends Migration
{
    public function up()
    {
        $this->createTable('airport', array(
            'id' => 'pk',
            'name' => 'string',
            'lat' => 'double',
            'lon' => 'double',
            'radius' => 'int',
            'cityId' => 'int'
        ));
    }

    public function down()
    {
        echo "m160110_160430_add_airports_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
