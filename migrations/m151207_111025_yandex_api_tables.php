<?php

use yii\db\Schema;
use yii\db\Migration;

class m151207_111025_yandex_api_tables extends Migration
{
    public function up()
    {
        $this->createTable('yandex_cookie', array(
            'id' => 'pk',
            'key' => 'string',
            'cookie' => 'text',
        ));
    }

    public function down()
    {
        echo "m151207_111025_yandex_api_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
