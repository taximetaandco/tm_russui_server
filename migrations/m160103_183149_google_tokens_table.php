<?php

use yii\db\Schema;
use yii\db\Migration;

class m160103_183149_google_tokens_table extends Migration
{
    public function up()
    {
        $this->createTable('google_distance_token', array(
            'id' => 'pk',
            'email' => 'string',
            'password' => 'string',
            'distance_key' => 'string'
        ));
    }

    public function down()
    {
        echo "m160103_183149_google_tokens_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
