<?php

use yii\db\Schema;
use yii\db\Migration;

class m160110_181110_add_cityid_column_to_tariffs extends Migration
{
    public function up()
    {
        $this->addColumn('service_tariff','cityId','int');
    }

    public function down()
    {
        echo "m160110_181110_add_cityid_column_to_tariffs cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
