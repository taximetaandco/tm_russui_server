<?php

use yii\db\Schema;
use yii\db\Migration;

class m160209_160826_add_token_column extends Migration
{
    public function up()
    {

        $this->addColumn('rider','token','text');
    }

    public function down()
    {
        echo "m160209_160826_add_token_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
