<?php

use yii\db\Schema;
use yii\db\Migration;

class m160215_180838_add_budget_table extends Migration
{
    public function up()
    {
        $this->createTable('budget', array(
            'id' => 'pk',
            'name' => 'string',
            'price' => 'int',
            'start' => 'date',
            'end' => 'date'
        ));
    }

    public function down()
    {
        echo "m160215_180838_add_budget_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
