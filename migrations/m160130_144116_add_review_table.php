<?php

use yii\db\Schema;
use yii\db\Migration;

class m160130_144116_add_review_table extends Migration
{
    public function up()
    {
    	$this->createTable('review', array(
            'id' => 'pk',
            'phone' => 'string',
            'created' => 'datetime',
            'text' => 'text',
            'rating' => 'int'
        ));
    }

    public function down()
    {
        echo "m160130_144116_add_review_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
