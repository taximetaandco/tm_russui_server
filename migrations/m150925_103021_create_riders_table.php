<?php

use yii\db\Schema;
use yii\db\Migration;

class m150925_103021_create_riders_table extends Migration
{
    public function up()
    {
        $this->createTable('rider', array(
            'id' => 'pk',
            'phone' => 'string',
            'created' => 'datetime',
            'name' => 'string',
            'lastLogin' => 'datetime'
        ));
    }

    public function down()
    {
        echo "m150925_103021_create_riders_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
