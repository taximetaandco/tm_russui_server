<?php

namespace app\models;


class ServiceTariff extends \yii\db\ActiveRecord
{
	public function scenarios()
    {
        return [
            'default' => ['serviceId', 'description', 'serviceName']
        ];
    }
}