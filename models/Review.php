<?php

namespace app\models;


class Review extends \yii\db\ActiveRecord
{

    public function scenarios()
    {
        return [
            'default' => ['phone', 'created', 'text', 'rating']
        ];
    }

    public static function tableName()
    {
        return 'review';
    }
}