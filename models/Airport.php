<?php

namespace app\models;


class Airport extends \yii\db\ActiveRecord
{
	public function scenarios()
    {
        return [
            'default' => ['radius', 'name', 'lat', 'lon', 'cityId']
        ];
    }
}