<?php

namespace app\models;


use app\models\Airport;

class City extends \yii\db\ActiveRecord
{
	public $services;
	public function scenarios()
    {
        return [
            'default' => ['radius', 'name', 'lat', 'lon']
        ];
    }

    public function getAirports()
    {
        return $this->hasMany(Airport::className(), ['cityId' => 'id']);
    }
}