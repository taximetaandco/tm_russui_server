<?php

namespace app\models;


class Budget extends \yii\db\ActiveRecord
{
	public function scenarios()
    {
        return [
            'default' => ['name', 'price', 'start', 'end']
        ];
    }
}