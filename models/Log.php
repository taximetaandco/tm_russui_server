<?php

namespace app\models;


class Log extends \yii\db\ActiveRecord
{
	public function scenarios()
    {
        return [
            'default' => ['created', 'ip', 'phone', 'type', 'data']
        ];
    }
}