<?php

namespace app\models;

class BudgetForm extends \yii\base\Model
{
    public $name;
    public $price;
    public $date;

    public function rules()
    {
        return [
            [['name', 'price', 'date'], 'required'],
            ['price', 'integer'],
            ['date', 'date'],
        ];
    }

    public function create()
    {
    	$budget = new Budget();
    	$budget->name=$this->name;
    	$budget->price=$this->price;
    	$budget->start=$this->date;
    	$budget->end=$this->date;

    	return $budget->save();
    }
}