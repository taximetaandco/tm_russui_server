<?php

namespace app\models;


class YandexCookie extends \yii\db\ActiveRecord
{
	public function scenarios()
    {
        return [
            'default' => ['key', 'cookie']
        ];
    }
}