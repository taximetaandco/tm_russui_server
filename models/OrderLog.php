<?php

namespace app\models;


class OrderLog extends \yii\db\ActiveRecord
{
    const  STATUS_ROUTING = 0;
    const  STATUS_CONFIRMED = 2;
    const  STATUS_UNKNOWN = 3;
    const  STATUS_WAITING = 4;
    const  STATUS_RIDING = 5;
    const  STATUS_FINISHED = 6; 
    const  STATUS_CANCELLED = 7;


    public function scenarios()
    {
        return [
            'default' => ['confirmed_time', 'canceled_time', 'created', 'lat', 'phone', 'lon', 'dstLat', 'dstLon', 'service', 'ip', 'comfort_class', 'price', 'status', 'due', 'driver_phone', 'taxi_service_name']
        ];
    }

    public static function tableName()
    {
        return 'order_log';
    }
}