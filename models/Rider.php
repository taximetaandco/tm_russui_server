<?php

namespace app\models;


class Rider extends \yii\db\ActiveRecord
{
	public function scenarios()
    {
        return [
            'default' => ['created', 'name', 'phone', 'lastLogin', 'platform']
        ];
    }
}