<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use app\models\OrderLog;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class StatController extends Controller
{
    private $accessCode = "C2DDF4YQHT5R5FRBZGHW";
    private $androidApiKey = "XM3RJ7YQSQFFJWDCW47M";
    private $iosApiKey = "5GVNCBRBCDP7RTTDGJR8";

    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionUpdateFlurry()
    {
        
      
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"http://api.flurry.com/appMetrics/ActiveUsers?apiAccessCode=".$this->accessCode."&apiKey=".$this->androidApiKey."&startDate=".date('Y-m-d', strtotime('-90 days'))."&endDate=".date("Y-m-d")."&groupBy=DAYS");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);
 
        curl_close ($ch);

        $response = json_decode($server_output)->{'day'};


        $resultActiveAndroid=[];
        foreach ($response as $row) {
             $resultActiveAndroid[]=[strtotime($row->{'@date'})*1000+3*60*60*1000,intval($row->{'@value'})];
        }

        sleep(1);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"http://api.flurry.com/appMetrics/ActiveUsers?apiAccessCode=".$this->accessCode."&apiKey=".$this->iosApiKey."&startDate=".date('Y-m-d', strtotime('-90 days'))."&endDate=".date("Y-m-d")."&groupBy=DAYS");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);
 
        curl_close ($ch);


        $response = json_decode($server_output)->{'day'};

        $resultActiveIOS=[];
        foreach ($response as $row) {
             $resultActiveIOS[]=[strtotime($row->{'@date'})*1000+3*60*60*1000,intval($row->{'@value'})];
        }

        $settings = \Yii::$app->settings;
        $settings->set('stat','android-active-users',json_encode($resultActiveAndroid));
        $settings->set('stat','ios-active-users',json_encode($resultActiveIOS));


        sleep(1);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"http://api.flurry.com/appMetrics/NewUsers?apiAccessCode=".$this->accessCode."&apiKey=".$this->androidApiKey."&startDate=".date('Y-m-d', strtotime('-180 days'))."&endDate=".date("Y-m-d")."&groupBy=DAYS");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);
 
        curl_close ($ch);

        $response = json_decode($server_output)->{'day'};


        $uniqueAndroid=0;
        foreach ($response as $row) {
             $uniqueAndroid+=intval($row->{'@value'});
        }

        sleep(1);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"http://api.flurry.com/appMetrics/NewUsers?apiAccessCode=".$this->accessCode."&apiKey=".$this->iosApiKey."&startDate=".date('Y-m-d', strtotime('-180 days'))."&endDate=".date("Y-m-d")."&groupBy=DAYS");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);
 
        curl_close ($ch);


        $response = json_decode($server_output)->{'day'};

        $uniqueIOS=0;
        foreach ($response as $row) {
             $uniqueIOS+=intval($row->{'@value'});
        }


        $settings->set('stat','android-unique-users',$uniqueAndroid);
        $settings->set('stat','ios-unique-users',2100+$uniqueIOS);
    }
}
