<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use app\models\OrderLog;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class OrderController extends Controller
{

    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionRefreshStatuses()
    {
        
      // compose the query  
 $connection = \Yii::$app->getDb();
$command = $connection->createCommand('UPDATE `order_log` set status=6 WHERE service != 6 and status = 0 and created > DATE_SUB(NOW(), INTERVAL 1 HOUR)');
 
$command->execute();


    }
}
