<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use app\models\GoogleDistanceToken;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class GoogleKeyController extends Controller
{

    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionCheckKeys()
    {
        
        $items = GoogleDistanceToken::find()->all();
        $i=0;
        foreach ($items as $item) {
        	$ch = curl_init();

	        curl_setopt($ch, CURLOPT_URL,"https://maps.googleapis.com/maps/api/distancematrix/json?origins=75+9th+Ave+New+York,+NY&destinations=Bridgewater+Commons,+Commons+Way,+Bridgewater,+NJ|The+Mall+At+Short+Hills,+Morris+Turnpike,+Short+Hills,+NJ|Monmouth+Mall,+Eatontown,+NJ|Westfield+Garden+State+Plaza,+Garden+State+Plaza+Boulevard,+Paramus,+NJ|Newport+Centre+Mall,+Jersey+City,+NJ&departure_time=now&traffic_model=best_guess&key=".$item->distance_key);

	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	        $server_output = curl_exec ($ch);



            $data = json_decode($server_output);

            echo $i++.'. '.$data->rows[0]->elements[0]->duration_in_traffic->value."\n";

            if(isset($data->error_message)){
                      echo $item->id.$data->error_message."\n\n";
                      }

     
           // echo json_encode($data->rows[0]->elements)."\n\n";
	 
	        curl_close ($ch);

        }
    }
}
