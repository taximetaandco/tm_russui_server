<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use app\models\OrderLog;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ExchangerController extends Controller
{
	public $upupkey = "3fa19d6e89da4b41baec5408874987f6";
    public $upupserver = "http://upup.su/api";

    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionRefreshStatuses()
    {
        
        $items = OrderLog::find()->where(['service'=>6])->andWhere(['not', ['inner_id' => null]])->andWhere(['status' => [OrderLog::STATUS_ROUTING, OrderLog::STATUS_CONFIRMED, OrderLog::STATUS_WAITING, OrderLog::STATUS_RIDING]])->orderBy(['id' => SORT_DESC])->all();
        foreach ($items as $item) {
        	$ch = curl_init();

        	$id=$item->inner_id;

	        curl_setopt($ch, CURLOPT_URL,"http://upup.su/api");
	        curl_setopt($ch, CURLOPT_POST, 1);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('v' => '1.5.13', 'key' => $this->upupkey, 'character' => 'utf-8', 'controller' => 'order', 'function' => 'info', 'id' => $id, 'json'=>1)));

	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	        $server_output = curl_exec ($ch);
	 
	        curl_close ($ch);

	        $json = json_decode($server_output, true);

	        $status = $json['order']['status'];

	        $status_code=0;
	        if($status == "start"||$status == "auction"){
	            $status_code = OrderLog::STATUS_ROUTING;
	        } else if($status == "work"||$status == "move"||$status == "client"){
	            $status_code = OrderLog::STATUS_CONFIRMED;
	        } else if($status == "client-move"){
	            $status_code = OrderLog::STATUS_RIDING;
	        } else  if($status == "done"){
	            $status_code = OrderLog::STATUS_FINISHED;
	        } else if($status == "cancel"||$status == "fail"||$status == "delete"){
	            $status_code = OrderLog::STATUS_CANCELLED;
	        }

	        $item->status=$status_code;
	        $item->save();
        }
    }
}
