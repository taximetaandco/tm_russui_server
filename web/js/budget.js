$(function () {

   

$.getJSON('http://cp.taximeta.ru/site/stat', function (data) {

    var buttons = Highcharts.getOptions().exporting.buttons.contextButton.menuItems;

buttons.push({
    text: "Day",
    onclick: function(){
        this.series.forEach(function(line) {
            line.update({
                dataGrouping: {
                        approximation: 'sum',
                        enabled: true,
                        forced: true,
                        units: [ ['day', [1] ] ]
                    }
                });
        });
    }
});
buttons.push({
    text: "Week",
    onclick: function(){
        this.series.forEach(function(line) {
            line.update({
                dataGrouping: {
                        approximation: 'sum',
                        enabled: true,
                        forced: true,
                        units: [ ['week', [1] ] ]
                    }
                });
        });
    }
});
buttons.push({
    text: "Month",
    onclick: function(){
        this.series.forEach(function(line) {
            line.update({
                dataGrouping: {
                        approximation: 'sum',
                        enabled: true,
                        forced: true,
                        units: [ ['month', [1] ] ]
                    }
                });
        });
    }
});
     
$('#budget').highcharts('StockChart', {

            rangeSelector: {
                selected: 1
            },

            title: {
                text: 'Advertising'
            },

            yAxis: [ {
              
                offset: 0,
                lineWidth: 2
            }],

             exporting: {
                buttons: {
                    contextButton: {
                        menuItems: buttons // or buttons.slice(0,6)
                    }
                }
            },

            series: [{
                name : 'rub',
                type: 'column',
                data: data.budget,
                yAxis: 0
            }]
        });

    });
});
