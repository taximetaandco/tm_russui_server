$(function () {

   

$.getJSON('http://cp.taximeta.ru/site/stat', function (data) {

    var buttons = Highcharts.getOptions().exporting.buttons.contextButton.menuItems;

buttons.push({
    text: "Day",
    onclick: function(){
        this.series.forEach(function(line) {
            line.update({
                dataGrouping: {
                        approximation: 'sum',
                        enabled: true,
                        forced: true,
                        units: [ ['day', [1] ] ]
                    }
                });
        });
    }
});
buttons.push({
    text: "Week",
    onclick: function(){
        this.series.forEach(function(line) {
            line.update({
                dataGrouping: {
                        approximation: 'sum',
                        enabled: true,
                        forced: true,
                        units: [ ['week', [1] ] ]
                    }
                });
        });
    }
});
buttons.push({
    text: "Month",
    onclick: function(){
        this.series.forEach(function(line) {
            line.update({
                dataGrouping: {
                        approximation: 'sum',
                        enabled: true,
                        forced: true,
                        units: [ ['month', [1] ] ]
                    }
                });
        });
    }
});
        // Create the chart
 // Create the chart
        $('#revenue').highcharts('StockChart', {
           
           
            rangeSelector : {
                selected : 1
            },

            title : {
                text : 'Revenue'
            },
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: buttons // or buttons.slice(0,6)
                    }
                }
            },
           
      
         series : [{
                name : 'rub',
                data : data.revenue,
                tooltip: {
                    valueDecimals: 2
                },
           
                    
            }],
         /*  plotOptions:{
                column:{
                    dataGrouping:{
                        enabled:false
                    }
                }
            }*/
        });

        var orderSeries = [];
        for (var service in data.orders['all']) {
          orderSeries.push({
                name : service,
                data : data.orders['all'][service],
                tooltip: {
                    valueDecimals: 2
                }
            });
        }

        $('#orders').highcharts('StockChart', {
            legend: {
                enabled:true,
                floating:true,
                layout:'vertical',
                verticalAlign:'middle',
                align:'left'
            },

            rangeSelector : {
                selected : 1
            },

            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: buttons // or buttons.slice(0,6)
                    }
                }
            },

            title : {
                text : 'Orders'
            },

            series : orderSeries,
         /*   plotOptions:{
                column:{
                    dataGrouping:{
                        enabled:false
                    }
                }
            }*/
        });

        var completedOrderSeries = [];
        for (var service in data.orders['completed']) {
          completedOrderSeries.push({
                name : service,
                data : data.orders['completed'][service],
                tooltip: {
                    valueDecimals: 2
                }
            });
        }

        $('#completed-orders').highcharts('StockChart', {
            legend: {
                enabled:true,
                floating:true,
                layout:'vertical',
                verticalAlign:'middle',
                align:'left'
            },

            rangeSelector : {
                selected : 1
            },

            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: buttons // or buttons.slice(0,6)
                    }
                }
            },

            title : {
                text : 'Completed orders'
            },

            series : completedOrderSeries,
        /*    plotOptions:{
                column:{
                    dataGrouping:{
                        enabled:false
                    }
                }
            }*/
        });

 $('#users').highcharts('StockChart', {
            legend: {
                enabled:true,
                floating:true,
                layout:'vertical',
                verticalAlign:'middle',
                align:'left'
            },

            rangeSelector : {
                selected : 1
            },



            title : {
                text : 'Active users'
            },

            series : [{
                name : 'Android',
                data : data.activeAndroid,
                tooltip: {
                    valueDecimals: 2
                }
            },{
                name : 'iOs',
                data : data.activeIOS,
                tooltip: {
                    valueDecimals: 2
                }
            }],
            plotOptions:{
                column:{
                    dataGrouping:{
                        enabled:false
                    }
                }
            }
        });
$('#budget').highcharts('StockChart', {

            rangeSelector: {
                selected: 1
            },

            title: {
                text: 'Advertising'
            },

            yAxis: [ {
              
                offset: 0,
                lineWidth: 2
            }],

             exporting: {
                buttons: {
                    contextButton: {
                        menuItems: buttons // or buttons.slice(0,6)
                    }
                }
            },

            series: [{
                type: 'column',
                data: data.budget,
                yAxis: 0
            }]
        });

 $('#new').highcharts('StockChart', {
            legend: {
                enabled:true,
                floating:true,
                layout:'vertical',
                verticalAlign:'middle',
                align:'left'
            },

            rangeSelector : {
                selected : 1
            },

            title : {
                text : 'New users'
            },

            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: buttons // or buttons.slice(0,6)
                    }
                }
            },

            series : [{
                name : 'Android',
                data : data.newAndroid,
                tooltip: {
                    valueDecimals: 2
                }
            },{
                name : 'iOs',
                data : data.newIOS,
                tooltip: {
                    valueDecimals: 2
                }
            }],
      /*      plotOptions:{
                column:{
                    dataGrouping:{
                        enabled:false
                    }
                }
            }*/
        });


 $('#times').highcharts('StockChart', {
            legend: {
                enabled:true,
                floating:true,
                layout:'vertical',
                verticalAlign:'middle',
                align:'left'
            },

            rangeSelector : {
                selected : 1
            },

            title : {
                text : 'Average time'
            },

            series : [{
                name : 'Confirm',
                data : data.times.confirmed,
                tooltip: {
                    valueDecimals: 2
                }
            }],
            plotOptions:{
                column:{
                    dataGrouping:{
                        enabled:false
                    }
                }
            }
        });

 initMap(data.points);
    });
});

var initMap = function(data){
    ymaps.ready(function () {
            var map = new ymaps.Map('YMapsID', {
                    center: [55.751082, 37.620243],
                    controls: ['zoomControl', 'typeSelector',  'fullscreenControl'],
                    zoom: 9, type: 'yandex#map'
                }),

                buttons = {
                  /*  dissipating: new ymaps.control.Button({
                        data: {
                            content: 'Toggle dissipating'
                        },
                        options: {
                            selectOnClick: false,
                            maxWidth: 150
                        }
                    }),*/
                    opacity: new ymaps.control.Button({
                        data: {
                            content: 'Change opacity'
                        },
                        options: {
                            selectOnClick: false,
                            maxWidth: 150
                        }
                    }),
                    radius: new ymaps.control.Button({
                        data: {
                            content: 'Change radius'
                        },
                        options: {
                            selectOnClick: false,
                            maxWidth: 150
                        }
                    }),
               /*     gradient: new ymaps.control.Button({
                        data: {
                            content: 'Reverse gradient'
                        },
                        options: {
                            selectOnClick: false,
                            maxWidth: 150
                        }
                    }),*/
                /*    heatmap: new ymaps.control.Button({
                        data: {
                            content: 'Toggle Heatmap'
                        },
                        options: {
                            selectOnClick: false,
                            maxWidth: 150
                        }
                    })*/
                },

                gradients = [{
                    0.1: 'rgba(128, 255, 0, 0.7)',
                    0.2: 'rgba(255, 255, 0, 0.8)',
                    0.7: 'rgba(234, 72, 58, 0.9)',
                    1.0: 'rgba(162, 36, 25, 1)'
                }, {
                    0.1: 'rgba(162, 36, 25, 0.7)',
                    0.2: 'rgba(234, 72, 58, 0.8)',
                    0.7: 'rgba(255, 255, 0, 0.9)',
                    1.0: 'rgba(128, 255, 0, 1)'
                }],
                radiuses = [5, 10, 20, 30],
                opacities = [0.4, 0.6, 0.8, 1];

            ymaps.modules.require(['Heatmap'], function (Heatmap) {
                var heatmap = new Heatmap(data, {
                    gradient: gradients[0],
                    radius: radiuses[1],
                    opacity: opacities[2]
                });
                heatmap.setMap(map);

               /* buttons.dissipating.events.add('press', function () {
                    heatmap.options.set(
                        'dissipating', !heatmap.options.get('dissipating')
                    );
                });*/
                buttons.opacity.events.add('press', function () {
                    var current = heatmap.options.get('opacity'),
                        index = opacities.indexOf(current);
                    heatmap.options.set(
                        'opacity', opacities[++index == opacities.length ? 0 : index]
                    );
                });
                buttons.radius.events.add('press', function () {
                    var current = heatmap.options.get('radius'),
                        index = radiuses.indexOf(current);
                    heatmap.options.set(
                        'radius', radiuses[++index == radiuses.length ? 0 : index]
                    );
                });
                /*buttons.gradient.events.add('press', function () {
                    var current = heatmap.options.get('gradient');
                    heatmap.options.set(
                        'gradient', current == gradients[0] ? gradients[1] : gradients[0]
                    );
                });
                buttons.heatmap.events.add('press', function () {
                    heatmap.setMap(
                        heatmap.getMap() ? null : map
                    );
                });*/

                for (var key in buttons) {
                    if (buttons.hasOwnProperty(key)) {
                        map.controls.add(buttons[key]);
                    }
                }
            });
        });
}

