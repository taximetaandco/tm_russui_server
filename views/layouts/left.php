<aside class="main-sidebar">

    <section class="sidebar">

     

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Dasboard', 'icon' => 'fa fa-file-code-o', 'url' => ['/site/index']],
                    ['label' => 'Analytics', 'icon' => 'fa fa-file-code-o', 'url' => ['/site/analytics']],
                    ['label' => 'Information', 'options' => ['class' => 'header']],
                    ['label' => 'Users', 'icon' => 'fa fa-file-code-o', 'url' => ['/site/users']],
                    ['label' => 'Orders', 'icon' => 'fa fa-dashboard', 'url' => ['/site/orders']],
                    ['label' => 'Reviews', 'icon' => 'fa fa-dashboard', 'url' => ['/site/reviews']],
                    ['label' => 'Settings', 'options' => ['class' => 'header']],
                    ['label' => 'Advertising', 'icon' => 'fa fa-dashboard', 'url' => ['/site/budget']],
                    ['label' => 'Services', 'icon' => 'fa fa-dashboard', 'url' => ['/site/tariffs']],
                    ['label' => 'Pushes', 'options' => ['class' => 'header']],
                    ['label' => 'Send', 'icon' => 'fa fa-file-code-o', 'url' => ['/push/send']],
                    ['label' => 'History', 'icon' => 'fa fa-dashboard', 'url' => ['/push/history']],
                ],
            ]
        ) ?>

    </section>

</aside>
