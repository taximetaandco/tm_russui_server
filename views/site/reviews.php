<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Reviews';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Phone</th>
                        <th>Time</th>
                        <th>Text</th>
                        <th>Rating</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($reviews as $review) { ?>
                      <tr>
                        <td><?= $review->phone ?></td>
                        <td><?= $review->created?></td>
                        <td><?= $review->text?></td>
                        <td><?= $review->rating?></td>
                      </tr>
                     <?php }?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Phone</th>
                        <th>Time</th>
                        <th>Text</th>
                        <th>Rating</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->
          </div><!-- /.row -->