<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Successful ($) orders ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Phone</th>
                        <th>Time</th>
                        <th>Price</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($orders as $order) { ?>
                      <tr>
                        <td><?= $order['phone'] ?></td>
                        <td><?= $order->created?></td>
                        <td><?= $order->price?></td>

                      </tr>
                     <?php }?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Phone</th>
                        <th>Time</th>
                        <th>Price</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->
          </div><!-- /.row -->