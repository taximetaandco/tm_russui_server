<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

$this->title = $tariff->serviceName;
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
	$id=$tariff->id;
	$form = ActiveForm::begin(['id' => 'tariff-form-'.$id, 'options'=>['class'=>'my-form']]);

?>
<?=$form->field($tariff, 'description')->widget(CKEditor::className(), [
	   	'editorOptions' => [
	        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
	        'inline' => false, //по умолчанию false
	    ],
    ]);?>
<div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end() ?>
<?php

?>
