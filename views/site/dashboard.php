<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('https://code.highcharts.com/stock/highstock.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('https://code.highcharts.com/stock/modules/exporting.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('//api-maps.yandex.ru/2.1/?lang=ru_RU', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/heatmap.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/stat.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<!-- Main row -->
         <div class="row">
            <!-- <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Requests/day</span>
                  <span class="info-box-number">90<small>%</small></span>
                </div>
              </div>
            </div>-->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-money"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Total revenue</span>
                  <span class="info-box-number"><?=$totalRevenue?> rub.</span>
                </div>
              </div>
            </div> 

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Orders (Completed/UpUp Completed/Total)</span>
                  <span class="info-box-number"><?=$finishedOrderCount?>/<?=$upupOrderCount?>/<?=$orderCount?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Members (iOs/Android)</span>
                  <span class="info-box-number"><?=$riderCount?> (<?=$riderIOSCount?>/<?=$riderAndroidCount?>)</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
              <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-blue"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Installs (iOs/Android)</span>
                  <span class="info-box-number"><?=($iosInstalls+$androidInstalls)?> (<?=$iosInstalls?>/<?=$androidInstalls?>)</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
       <div id="YMapsID" style="height: 400px; min-width: 310px"></div>
<div id="revenue" style="height: 400px; min-width: 310px"></div>
<div id="completed-orders" style="height: 400px; min-width: 310px"></div>
<div id="orders" style="height: 400px; min-width: 310px"></div>
<div id="users" style="height: 400px; min-width: 310px"></div>
<div id="new" style="height: 400px; min-width: 310px"></div>
<div id="budget" style="height: 400px; min-width: 310px"></div>
<div id="times" style="height: 400px; min-width: 310px"></div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->
          </div><!-- /.row -->