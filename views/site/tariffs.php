<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('@web/js/orders.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                 
                  <label>Upnup exchanger is
    <label id="CheckBoxValue" value="None"></label>
</label>
 <input id="TheCheckBox" type="checkbox" name="my-checkbox" checked>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->
          </div><!-- /.row -->
<h3>Москва</h3>
<div class="btn-group-vertical" role="group" aria-label="Vertical button group">
<?php
foreach ($moscowTariffs as $tariff) {
	$id=$tariff->id;
?>
<?= Html::a($tariff->serviceName, ['/site/tariff/'.$tariff->id],['class' => 'btn btn-default']) ?>
<?php

}

?>
 
 </div>
 <h3>Санкт-Петербург</h3>
<div class="btn-group-vertical" role="group" aria-label="Vertical button group">
<?php
foreach ($piterTariffs as $tariff) {
  $id=$tariff->id;
?>
<?= Html::a($tariff->serviceName, ['/site/tariff/'.$tariff->id],['class' => 'btn btn-default']) ?>
<?php

}

?>
 
 </div>