<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Advertising settings';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('https://code.highcharts.com/stock/highstock.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('https://code.highcharts.com/stock/modules/exporting.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('//api-maps.yandex.ru/2.1/?lang=ru_RU', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/budget.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<!-- Main row -->
 
    
            
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
<div id="budget" style="height: 400px; min-width: 310px"></div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->
          </div><!-- /.row -->


<div class="row">
 <div class="col-xs-12">
              <div class="box">
                <div class="box-body">

             <?php   $form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => ['class' => 'form-inline'],
]) ?>

        <div class="form-group">


<?= $form->field($model, 'name', array('template'=>"{input}", 'inputOptions'=>array('placeholder'=>'Name', 'class'=>'form-control')))->textInput()->hint(false)->label(false) ?>
 
        </div>

        <div class="form-group">

<?= $form->field($model, 'price', array('template'=>"{input}", 'inputOptions'=>array('placeholder'=>'Price', 'class'=>'form-control')))->textInput()->hint(false)->label(false) ?>
 

        </div>

         <div class="form-group">
<?= $form->field($model, 'date', array('template'=>"{input}", 'inputOptions'=>array('placeholder'=>'Date', 'class'=>'form-control')))->input('date')->hint(false)->label(false) ?>
 


        </div>
 <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>

  <?php ActiveForm::end() ?>
       </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->
</div>

<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Date</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($items as $item) { ?>
                      <tr>
                        <td><?= $item->name ?></td>
                        <td><?=$item->price?> rub.</td>
                        <td><?=$item->start?></td>
                        <td><?= Html::a("Delete", ['/site/deletebudget/'.$item->id]) ?></td>

                      </tr>
                     <?php }?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Date</th>
                        <th>Actions</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->
          </div><!-- /.row -->
