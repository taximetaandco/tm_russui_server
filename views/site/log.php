<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'Rider app logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
            <div class="col-xs-12">
                <?=ListView::widget( [
                    'dataProvider' => $dataProvider,
                    'itemView' => '_logitem',
                    'itemOptions' => ['class'=>'box-body'],
                    'options' =>['class'=>'box']
                ] ); ?>

              
            </div><!-- /.col -->
          </div><!-- /.row -->