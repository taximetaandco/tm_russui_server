<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Phone</th>
                        <th>Name</th>
                        <th>Created</th>
                        <th>Last login</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($riders as $rider) { ?>
                      <tr>
                        <td><?= Html::a($rider->phone, ['/site/log/'.$rider->phone]) ?></td>
                        <td><?=$rider->name?></td>
                        <td><?=$rider->created?></td>
                        <td><?=$rider->lastLogin?></td>

                      </tr>
                     <?php }?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Phone</th>
                        <th>Name</th>
                        <th>Created</th>
                        <th>Last login</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->
          </div><!-- /.row -->