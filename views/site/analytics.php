<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Analytics';
$this->params['breadcrumbs'][] = $this->title;
?>
  <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Active riders / active exchanger users/ installs</span>
                  <span class="info-box-number"><?=$activeUsers?>/<?=$exchangerUsers?>/<?=$installs?></span>
                </div>
              </div>
            </div> 

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Returning riders </span>
                  <span class="info-box-number"><?=$retRiders?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-money"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Avg revenue per user</span>
                  <span class="info-box-number"><?=$avgRevenue?> rub.</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
             <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-blue"><i class="fa fa-money"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Avg cost/rev per ride</span>
                  <span class="info-box-number"><?=$avgCPR?>/<?=intval($avgCPR*0.08)?> rub.</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
          </div>

<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Phone</th>
                        <th>Rides</th>
                        <th>Revenue</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($riders as $rider) { ?>
                      <tr>
                        <td><?= $rider['phone'] ?></td>
                        <td><?= $rider['rides']?></td>
                        <td><?= $rider['price']?></td>

                      </tr>
                     <?php }?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Phone</th>
                        <th>Rides</th>
                        <th>Revenue</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              
            </div><!-- /.col -->
          </div><!-- /.row -->